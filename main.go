package main

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	article "bitbucket.org/b0ralgin/micro_API/article"
	task "bitbucket.org/b0ralgin/micro_API/task"
	"github.com/PuerkitoBio/goquery"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-plugins/broker/nats"
	natsio "github.com/nats-io/nats"
)

type HtmlAggregator struct {
	client *http.Client
}

func NewHtmlAggregator() *HtmlAggregator {
	return &HtmlAggregator{
		client: &http.Client{
			Timeout:   time.Second * 10,
			Transport: &http.Transport{},
		},
	}
}
func (h *HtmlAggregator) FetchData(artChan chan<- article.Article) func(ctx context.Context, req *task.Task) error {
	return func(ctx context.Context, req *task.Task) error {
		siteUrl := req.GetUrl()
		resp, err := h.client.Get(siteUrl)
		if resp != nil {
			defer resp.Body.Close()
		}
		if err != nil {
			return err
		}
		doc, err := goquery.NewDocumentFromReader(resp.Body)
		if err != nil {
			return err
		}
		var wg sync.WaitGroup
		doc.Find(req.LinkSelector).Each(func(i int, s *goquery.Selection) {
			if articleUrl, exists := s.Attr("href"); exists {
				wg.Add(1)

				go func() {
					base, _ := url.Parse(siteUrl)
					linkUrl, _ := url.Parse(articleUrl)

					article, err := h.parseArticle(
						h.client,
						base.ResolveReference(linkUrl).String(),
						req.TitleSelector,
						req.ContentSelector,
						req.PublishSelector,
					)

					if err != nil {
						log.Println("Cannot parse article", err)
					} else {
						article.Source = siteUrl
						artChan <- article
					}

					wg.Done()
				}()
			}
		})
		wg.Wait()
		return nil
	}
}

func (h *HtmlAggregator) parseArticle(client *http.Client, url, titleS, contentS, publishS string) (article.Article, error) {
	newArticle := article.Article{}
	resp, err := h.client.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		log.Println(err)
		return newArticle, err
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return newArticle, err
	}
	title := doc.Find(titleS).Text()
	content := doc.Find(contentS).Text()
	publishAt, err := time.Parse("hh.mm", doc.Find(publishS).Text())
	if err == nil {
		now := time.Now()
		publishAt.AddDate(now.Year(), int(now.Month()), now.Day())
	} else {
		publishAt = time.Now()
	}
	newArticle = article.Article{
		Title:       title,
		Content:     content,
		PublishedAt: publishAt.Unix(),
	}
	return newArticle, nil
}

func sendToDB(client client.Client, dbQueue string, queue chan article.Article) {
	p := micro.NewPublisher(dbQueue, client)
	for article := range queue {
		p.Publish(context.Background(), &article)
	}
}

func main() {
	natsQueue := os.Getenv("NATS_QUEUE")
	dbQueue := os.Getenv("DB_QUEUE")
	natsServer := os.Getenv("NATS_SERVER")
	article := make(chan article.Article)
	if natsQueue == "" || natsServer == "" {
		log.Fatal("wrong Envs")
	}
	broker := nats.NewBroker(nats.Options(natsio.Options{
		Url: natsServer,
	}))
	srv := micro.NewService(
		micro.Name("go.micro.html"),
		micro.Broker(broker),
	)

	srv.Init()
	aggregator := NewHtmlAggregator()
	err := micro.RegisterSubscriber(natsQueue, srv.Server(), aggregator.FetchData(article))
	if err != nil {
		log.Fatal(err)
	}
	go sendToDB(srv.Client(), dbQueue, article)
	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
