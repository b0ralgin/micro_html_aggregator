FROM b0ralgin/glide

ENV APP_PATH=/go/src/bitbucket.org/b0ralgin/micro_html_aggregator/

RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

COPY glide.yaml glide.yaml
COPY glide.lock glide.lock
RUN glide install -v

ADD . $APP_PATH
RUN CGO_ENABLED=0 go build -o html
CMD ./html